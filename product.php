<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login");
    exit;
}
 
// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$title = $description = "";
$id = $x = $y = $z = $quantity = "";
$params_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sql = "INSERT INTO Orders (PID, Username, X, Y, Z, Quantity) VALUES (?, ?, ?, ?, ?, ?)";
        if ($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "isiiii", $_POST["id"], $_SESSION["username"], $_POST["x"], $_POST["y"], $_POST["z"], $_POST["quantity"]);

            // Set parameters
            $param_id = $_POST["id"];
            $param_username = $_SESSION["username"];
            $param_x = $_POST["x"];
            $param_y = $_POST["y"];
            $param_z = $_POST["z"];
            $param_quantity = $_POST["quantity"];
            
            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)){
                // Redirect to welcome page
                header("location: welcome");
            } else{
                echo "Something went wrong. Please try again later.";
            }
            // Close statement
            mysqli_stmt_close($stmt);
        }
    
}
elseif ($_SERVER["REQUEST_METHOD"] == "GET" && is_numeric($_GET['id'])) {
    $result = mysqli_query($link, 'SELECT * FROM products WHERE id = ' . $_GET['id']);
    if ($row = mysqli_fetch_array($result)) {
        $id = $row['id'];
        $title = $row['title'];
        $description = $row['data'];
    }
}
else {
    die();
}

mysqli_close($link);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Catalog - AK CONSULT</title>
        <link rel="stylesheet" href="static/main.css"/>
    </head>
    <body>
        <nav class="container navbar navbar-light navbar-expand-sm">
            <div class="login">
                <span class="navbar-text">
                    Logged in as <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b> — <a href="logout">Log out</a>
                </span>
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section>
                        <p>You can browse our catalog here.</p>
                    </section>
                </div>
            </div>
        </div>
        <div class="header-tabbed">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" href="profile">profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="catalog">catalog</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <h3><?php echo $title; ?><h3>
                        <p><?php echo $description; ?></p>
                        <form method="POST">
                            <div class="form-group">
                                <label for="x">X <span class="text-danger">*</span></label>
                                <input type="number" name="x" id="x" value="<?php echo $params_x; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="y">Y <span class="text-danger">*</span></label>
                                <input type="number" name="y" id="y" value="<?php echo $params_y; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="z">Z <span class="text-danger">*</span></label>
                                <input type="number" name="z" id="z" value="<?php echo $params_z; ?>" class="form-control" required autofocus />
                            </div>

                            <div class="form-group">
                                <label for="quantity">Quantity <span class="text-danger">*</span></label>
                                <input type="number" name="quantity" id="quantity" value="<?php echo $params_quantity; ?>" class="form-control" required autofocus />
                            </div>

                            <input type="hidden" name="id" value="<?php echo $id; ?>">

                            <div class="alert alert-danger"><?php echo $params_err; ?></div>

                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
    </body>
</html>
